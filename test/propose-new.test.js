const test = require('tape')
const { promisify: p } = require('util')
const { isMsgId } = require('ssb-ref')

const Server = require('./test-bot')

/**
 * TODO cherese 29/03/23
 * Need to add tests for more cases:
 * - [ ] recordDetails
 *   - [ ] authors
 *   - [ ] recps
 * - [ ] submissionDetails
 *   - [ ] recps
 */

const defaultValidFields = {
  recordTypeContent: { type: 'profile/person' },
  recordDetails: {
    preferredName: 'Colin'
    // authors: { add: [ssb.id] }
  },
  submissionDetails: {
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
  }
}

test('proposeNew simple (profile/person public)', async t => {
  t.plan(2)
  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const recordTypeContent = { type: 'profile/person' }

  const recordDetails = {
    preferredName: 'Cherese',
    gender: 'female'
  }

  const submissionDetails = {
    comment: 'Can you please add my record?',
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
  }

  const submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)
  t.true(isMsgId(submissionId), 'submissionId is a valid messageId')

  const submission = await p(ssb.submissions.get)(submissionId)

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: null,
      targetId: null,
      targetType: 'profile/person',
      details: recordDetails,
      conflictFields: [],
      tombstone: null,
      approvedBy: [],
      rejectedBy: [],
      comments: {
        [ssb.id]: 'Can you please add my record?'
      },
      states: [
        {
          key: submissionId,
          targetId: null,
          tombstone: null,
          approvedBy: [],
          rejectedBy: [],
          comments: {
            [ssb.id]: 'Can you please add my record?'
          }
        }
      ]
    },
    'returns the correct submission'
  )
})

test('proposeNew invalid field inputs (profile/person public)', async t => {
  t.plan(18)

  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  /**
   * Failing test cases
   */

  const failCases = [
    // invalid recordTypeContent cases
    {
      recordTypeContent: null,
      expectedError: /submissions.proposeNew recordTypeContent is missing/
    },
    {
      recordTypeContent: undefined,
      expectedError: /submissions.proposeNew recordTypeContent is missing/
    },
    {
      recordTypeContent: false,
      expectedError: /submissions.proposeNew recordTypeContent is missing/
    },
    {
      recordTypeContent: 'not a valid type content',
      expectedError: /submissions.proposeNew recordTypeContent must be an object/
    },
    {
      recordTypeContent: ['not a valid type'],
      expectedError: /submissions.proposeNew recordTypeContent must be an object/
    },
    {
      recordTypeContent: { type: 'not a valid type' },
      expectedError: /No handler found for the recordTypeContent: {"type":"not a valid type"}/
    },
    {
      recordTypeContent: { invalid: 'not a valid type' },
      expectedError: /No handler found for the recordTypeContent: {"invalid":"not a valid type"}/
    },
    {
      recordTypeContent: { type: 'whakapapa/view' },
      expectedError: /No handler found for the recordTypeContent: {"type":"whakapapa\/view"}/
    },

    // invalid recordDetails cases
    {
      recordDetails: null,
      expectedError: /submissions.proposeNew recordDetails must be an object/
    },
    {
      recordDetails: undefined,
      expectedError: /submissions.proposeNew recordDetails must be an object/
    },
    {
      recordDetails: false,
      expectedError: /submissions.proposeNew recordDetails must be an object/
    },
    {
      recordDetails: ['not valid details'],
      expectedError: /submissions.proposeNew recordDetails must be an object/
    },
    {
      recordDetails: { causes: 'error' },
      expectedError: /submissions.proposeNew recordDetails has unallowed inputs: ["causes"]/
    },

    // profile/person recordDetails - validate against spec
    // TODO: this only tests the profile/person case, we may need to test other crut types?
    {
      recordTypeContent: { type: 'profile/person' },
      recordDetails: {
        preferredName: 'Cherese',
        legalName: 'Cherese Eriepa' // invalid against the profile/person public spec
      },
      expectedError: /submissions.proposeNew recordDetails has unallowed inputs: legalName/
    },

    // invalid submissionDetails
    {
      submissionDetails: null,
      expectedError: /submissions.proposeNew submissionDetails must be an object/
    },
    {
      submissionDetails: undefined,
      expectedError: /submissions.proposeNew submissionDetails must be an object/
    },
    {
      submissionDetails: false,
      expectedError: /submissions.proposeNew submissionDetails must be an object/
    },
    {
      submissionDetails: { dog: 'coco' },
      expectedError: /submissions.proposeNew submissionDetails has unallowed inputs: dog/
    }
    // {
    //   submissionDetails: {},
    //   expectedError: /data.groupId is required/
    // }
  ]

  /**
   * Run the failing test cases
   */

  await Promise.all(
    failCases.map(async input => {
      const {
        recordTypeContent,
        recordDetails,
        submissionDetails,
        expectedError
      } = ({ ...defaultValidFields, ...input })

      delete input.expectedError

      return p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)
        .then(() => t.fail(`expected ${expectedError} to fail, but it passed instead`))
        .catch(err => t.match(err.message, expectedError, `invalid input ${JSON.stringify(input)} throws error`))
    })
  )
})
