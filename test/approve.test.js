const test = require('tape')
const { promisify: p } = require('util')
const Server = require('./test-bot')

test('approve invalid field inputs', async t => {
  t.plan(11)

  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  // const profileId = await p(ssb.profile.person.public.create)({ authors: { add: [ssb.id] } })
  const recordTypeContent = {
    type: 'profile/person'
  }

  const recordDetails = {
    preferredName: 'Cherese'
  }

  const submissionDetails = {
    comment: 'Create a new profile',
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
  }

  const submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)
  // const communityProfileId = await p(ssb.profile.community.public.create)({ authors: { add: [ssb.id] } })

  const defaultValidFields = {
    submissionId,
    submissionDetails: {
      comment: 'boop!',
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
    }
  }

  /**
   * Failing test cases
   */

  const failCases = [
    // invalid recordId cases
    {
      submissionId: null,
      expectedError: /submissions.approve submissionId is missing/
    },
    {
      submissionId: undefined,
      expectedError: /submissions.approve submissionId is missing/
    },
    {
      submissionId: false,
      expectedError: /submissions.approve submissionId is missing/
    },
    { // invalid string value for recordId
      submissionId: 'not a valid submissionId',
      expectedError: /submissions.approve submissionId must be a valid msgId/
    },
    { // invalid array value for recordId
      submissionId: ['not a valid submissionId'],
      expectedError: /submissions.approve submissionId must be a valid msgId/
    },
    { // invalid object value for recordId
      submissionId: { submissionId: 'not a valid submissionId' },
      expectedError: /submissions.approve submissionId must be a valid msgId/
    },

    // valid recordId, but doesnt exist
    {
      submissionId: '%R+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256',
      expectedError: /submissions.approve no submission found for submissionId: %R\+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256/
    },

    // TODO: do we need to test for a handler here
    // valid recordId, but no handler registered
    // {
    //   recordId: communityProfileId,
    //   expectedError: /submissions.proposeEdit Error: No CRUT registered for type profile\/community/
    // },

    // invalid submissionDetails
    {
      submissionDetails: null,
      expectedError: /submissions.approve submissionDetails must be an object/
    },
    {
      submissionDetails: undefined,
      expectedError: /submissions.approve submissionDetails must be an object/
    },
    {
      submissionDetails: false,
      expectedError: /submissions.approve submissionDetails must be an object/
    },
    {
      submissionDetails: { dog: 'coco' },
      expectedError: /submissions.approve submissionDetails has unallowed inputs: dog/
    }
  ]

  /**
   * Run the failing test cases
   */

  await Promise.all(
    failCases.map(async input => {
      const {
        submissionId,
        submissionDetails,
        expectedError
      } = ({ ...defaultValidFields, ...input })

      delete input.expectedError

      return p(ssb.submissions.approve)(submissionId, submissionDetails)
        .then(() => t.fail(`expected ${expectedError} to fail, but it passed instead`))
        .catch(err => t.match(err.message, expectedError, `submission.proposeEdit invalid input ${JSON.stringify(input)} throws error`))
    })
  )
})

const details = () => ({
  preferredName: 'Colin'
})

test('approve submission created with proposeEdit', async t => {
  const ssb = Server()
  t.teardown(ssb.close)

  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [ssb.id] }
  }

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const profileId = await p(ssb.profile.person.public.create)(profileDetails)
  const submissionId = await p(ssb.submissions.proposeEdit)(profileId, details(), { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  await p(ssb.submissions.approve)(submissionId, {})

  const submission = await p(ssb.submissions.get)(submissionId)
  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      details: details(),
      conflictFields: [],
      states: [{
        key: submission.states[0].key,
        targetId: null,
        tombstone: null,
        approvedBy: [ssb.id],
        rejectedBy: [],
        comments: {}
      }],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {}
    },
    'the submission was approved'
  )
})

test('approve submission created with proposeNew', async t => {
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const submissionId = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, details(), { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  await p(ssb.submissions.approve)(submissionId, {})

  const submission = await p(ssb.submissions.get)(submissionId)

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: null, // No sourceId yet as approve does not handle creating the record
      targetId: null,
      targetType: 'profile/person',
      details: details(),
      conflictFields: [],
      states: [{
        key: submission.states[0].key,
        targetId: null,
        tombstone: null,
        approvedBy: [ssb.id],
        rejectedBy: [],
        comments: {}
      }],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {}
    },
    'the submission was approved'
  )
})

test('approve submission created with proposeTombstone', async t => {
  const ssb = Server()
  t.teardown(ssb.close)

  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [ssb.id] }
  }

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const profileId = await p(ssb.profile.person.public.create)(profileDetails)
  const submissionId = await p(ssb.submissions.proposeTombstone)(profileId, { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  await p(ssb.submissions.approve)(submissionId, {})

  const submission = await p(ssb.submissions.get)(submissionId)
  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      details: {
        tombstone: {
          date: submission.details.tombstone.date
        }
      },
      conflictFields: [],
      states: [{
        key: submission.states[0].key,
        targetId: null,
        tombstone: null,
        approvedBy: [ssb.id],
        rejectedBy: [],
        comments: {}
      }],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {}
    },
    'the submission was approved'
  )
})
