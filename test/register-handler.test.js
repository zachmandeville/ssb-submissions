const test = require('tape')
const Server = require('./test-bot')

const details = () => ({
  preferredName: 'Colin'

})

test('submissions.registerHandler', t => {
  const server = Server()
  const kaitiaki = server.id
  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [kaitiaki] }
  }

  // server.submissions.registerHandler(server.profile.person.public)
  t.throws(() => {
    server.submissions.registerHandler(null)
  }, /crut should not be null/, 'crut should not be null')
  t.throws(() => {
    server.submissions.registerHandler('crut')
  }, /Invalid crut registered as handler/, 'Invalid crut registered as handler')

  server.profile.person.public.create(profileDetails, (err, profileId) => {
    t.error(err, 'Profile created')
    server.submissions.proposeEdit(profileId, details(), {}, (err, submissionId) => {
      t.deepEquals(err, new Error('submissions.proposeEdit Error: No CRUT registered for type profile/person'), 'no handlers registered')
      server.submissions.registerHandler(server.profile.person.admin)
      server.submissions.proposeEdit(profileId, details(), {}, (err, submissionId) => {
        t.deepEquals(err, new Error('submissions.proposeEdit Error: No CRUT registered for type profile/person'), 'incorrect handler registered')
        server.close()
        t.end()
      })
    })
  })
})
