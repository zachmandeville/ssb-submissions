const test = require('tape')
const { promisify: p } = require('util')
const { isMsgId } = require('ssb-ref')

const Server = require('./test-bot')

test('submission.tombstone', async t => {
  t.plan(3)

  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const recordTypeContent = {
    type: 'profile/person'
  }

  const recordDetails = {
    preferredName: 'Colin'
  }

  const submissionDetails = {
    // comment,
    // recps
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
  }

  const submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)
  t.true(isMsgId(submissionId), 'created a new proposal submission')

  // tombstone the submission
  await p(ssb.submissions.tombstone)(submissionId, { reason: 'woops' })
    .then(() => t.pass('the submission was tombstoned'))

  const submission = await p(ssb.submissions.get)(submissionId)

  const state = {
    tombstone: {
      date: submission.tombstone.date, // hack because we cannot determine this
      reason: 'woops'
    },
    approvedBy: [],
    rejectedBy: [],
    comments: {}
  }

  const expected = {
    key: submissionId,
    type: 'submissions',
    originalAuthor: ssb.id,
    recps: null,
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
    sourceId: null,
    targetId: null,
    targetType: 'profile/person',
    details: { preferredName: 'Colin' },
    states: [
      {
        key: submission.states[0].key, // hack because we dont get back this key
        targetId: null,
        ...state
      }
    ],
    conflictFields: [],
    ...state
  }

  t.deepEqual(submission, expected, 'returns the updated submission that was tombstoned')
})

test('submission.tombstone invalid submissionId', async t => {
  t.plan(6)

  const ssb = Server()
  t.teardown(ssb.close)

  const failCases = [
    {
      submissionId: null,
      expectedError: /submissions.tombstone submissionId is missing/
    },
    {
      submissionId: undefined,
      expectedError: /submissions.tombstone submissionId is missing/
    },
    {
      submissionId: false,
      expectedError: /submissions.tombstone submissionId is missing/
    },

    // invalid values
    {
      submissionId: true,
      expectedError: /submissions.tombstone submissionId must be a valid msgId/
    },
    {
      submissionId: 'boop!',
      expectedError: /submissions.tombstone submissionId must be a valid msgId/
    },

    // valid msgId, non-existing submission
    {
      submissionId: '%R+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256',
      expectedError: /submissions.tombstone no submission found for submissionId: %R\+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256/
    }
  ]

  /**
   * Run the failing test cases
   */

  await Promise.all(
    failCases.map(async ({ submissionId, expectedError }) => {
      return p(ssb.submissions.tombstone)(submissionId, { reason: 'woops' })
        .then(() => t.fail(`expected invalid submissionId ${submissionId} to fail, but it passed instead`))
        .catch(err => {
          t.match(err.message, expectedError, `invalid input ${JSON.stringify(submissionId)} throws error`)
        })
    })
  )
})

test('approve tombstoned submissions', async t => {
  const ssb = Server()
  t.teardown(ssb.close)

  function recordDetails () {
    return {
      preferredName: 'Col',
      authors: {
        add: [ssb.id]
      }
    }
  }

  ssb.submissions.registerHandler(ssb.profile.person.public)
  const profileId = await p(ssb.profile.person.public.create)(recordDetails())

  // testing a submission created with proposeTombstone
  let submissionId = await p(ssb.submissions.proposeTombstone)(profileId, { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  await p(ssb.submissions.tombstone)(submissionId, {})
  await p(ssb.submissions.approve)(submissionId, {})

  let submission = await ssb.submissions.get(submissionId)
  t.true(submission.details.tombstone && submission.details.tombstone.date, 'tombstone details were added to the submission')

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      details: {
        // from submission.proposeTombstone
        tombstone: submission.details.tombstone
      },
      states: [
        {
          key: submission.states[0].key,
          targetId: null,

          // from submission.tombstone
          tombstone: {
            date: submission.tombstone.date
            // reason: null
          },
          approvedBy: [ssb.id], // TODO: shouldnt be able to approve your own submission
          rejectedBy: [],
          comments: {}
        }
      ],
      conflictFields: [],

      // from submission.tombstone
      tombstone: {
        date: submission.tombstone.date
        // reason: null
      },
      approvedBy: [ssb.id], // TODO: shouldnt be able to approve your own submission
      rejectedBy: [],
      comments: {}
    },
    'gets the tombstoned submission and proposeTombstone details'
  )

  // testing a submission created with proposeNew
  const recordTypeContent = {
    type: 'profile/person'
  }

  // TODO: proposeNew not working with authors on recordDetails
  submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, { preferredName: 'Colin' }, { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' })
  await p(ssb.submissions.tombstone)(submissionId, {})
  await p(ssb.submissions.approve)(submissionId, {})
  submission = await ssb.submissions.get(submissionId)

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
      sourceId: submission.sourceId,
      targetId: null,
      targetType: 'profile/person',
      details: {
        preferredName: 'Colin'
      },
      states: [
        {
          key: submission.states[0].key,
          targetId: null,
          tombstone: {
            date: submission.tombstone.date
            // reason: null
          },
          approvedBy: [ssb.id], // TODO: shouldnt be able to approve your own submission
          rejectedBy: [],
          comments: {}
        }
      ],
      conflictFields: [],
      tombstone: {
        date: submission.tombstone.date
        // reason: null
      },
      approvedBy: [ssb.id], // TODO: shouldnt be able to approve your own submission
      rejectedBy: [],
      comments: {}
    },
    'gets the tombstoned submission and proposeNew details'
  )

  t.end()
})
