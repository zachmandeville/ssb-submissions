const test = require('tape')
const { promisify: p } = require('util')
const { isMsgId } = require('ssb-ref')
const CrutByType = require('ssb-whakapapa/lib/crut-by-type')

const Server = require('./test-bot')
const { TestGroup } = require('./lib/helpers')

const groupId = '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'

test('proposeNew, proposeEdit, executeAndApprove (public profile/person)', async t => {
  t.plan(13)
  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const recordTypeContent = { type: 'profile/person' }

  const recordDetails = {
    preferredName: 'Cherese',
    gender: 'female'
  }

  const submissionDetails = {
    comment: 'Can you please add my record?',
    groupId
  }

  const submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)
  t.true(isMsgId(submissionId), 'submissionId is a valid messageId')

  let submission = await p(ssb.submissions.get)(submissionId)

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId,
      sourceId: null,
      targetId: null,
      targetType: 'profile/person',
      details: recordDetails,
      conflictFields: [],
      tombstone: null,
      approvedBy: [],
      rejectedBy: [],
      comments: {
        [ssb.id]: 'Can you please add my record?'
      },
      states: [
        {
          key: submissionId,
          targetId: null,
          tombstone: null,
          approvedBy: [],
          rejectedBy: [],
          comments: {
            [ssb.id]: 'Can you please add my record?'
          }
        }
      ]
    },
    'returns the correct submission'
  )

  const approvedRecordDetails = {
    preferredName: 'Cherese'
    // gender: 'female'
  }

  // execute
  await p(ssb.submissions.executeAndApprove)(submissionId, approvedRecordDetails, { comment: 'Yup' })

  submission = await p(ssb.submissions.get)(submissionId)

  const profileId = submission.targetId
  t.true(profileId, 'there is a profileId')

  t.notDeepEqual(submissionId, submission.states[0].key, 'an update was published to the submission')

  t.deepEqual(
    submission,
    {
      key: submissionId,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId,
      sourceId: null,
      targetId: profileId,
      targetType: 'profile/person',
      details: recordDetails,
      conflictFields: [],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {
        [ssb.id]: 'Yup'
      },
      states: [
        {
          key: submission.states[0].key,
          targetId: profileId,
          tombstone: null,
          approvedBy: [ssb.id],
          rejectedBy: [],
          comments: {
            [ssb.id]: 'Yup'
          }
        }
      ]
    },
    'returns the correct submission after execute'
  )

  // get the profile
  let profile = await p(ssb.profile.person.public.get)(profileId)

  t.deepEquals(
    profile,
    {
      key: profileId,
      type: 'profile/person',
      ...recordDetails,
      originalAuthor: ssb.id,
      recps: null,
      conflictFields: [],
      states: [
        {
          key: profileId,
          tombstone: null,
          ...recordDetails,
          avatarImage: null,
          gender: null, // expect to be null because we didnt pass this value into
          authors: profile.authors,
          type: 'person'
        }
      ],
      tombstone: null,
      avatarImage: null,
      gender: null,
      authors: profile.authors
    },
    'the profile was created'
  )

  const updateDetails = { gender: 'female' }

  // propose an edit to the profile
  const submissionId2 = await p(ssb.submissions.proposeEdit)(profileId, updateDetails, { comment: 'Please update this gender', groupId })

  submission = await p(ssb.submissions.get)(submissionId2)

  t.deepEqual(
    submission,
    {
      key: submissionId2,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId,
      sourceId: profileId,
      targetId: null,
      targetType: 'profile/person',
      details: updateDetails,
      conflictFields: [],
      tombstone: null,
      approvedBy: [],
      rejectedBy: [],
      comments: {
        [ssb.id]: 'Please update this gender'
      },
      states: [
        {
          key: submissionId2,
          targetId: null,
          tombstone: null,
          approvedBy: [],
          rejectedBy: [],
          comments: {
            [ssb.id]: 'Please update this gender'
          }
        }
      ]
    },
    'returns the correct submission to update'
  )

  await p(ssb.submissions.executeAndApprove)(submissionId2, updateDetails, { comment: 'Yup' })

  submission = await p(ssb.submissions.get)(submissionId2)

  t.deepEquals(submission.sourceId, profileId, 'sourceId is the original profileId')
  t.true(submission.targetId, 'there is a profileId')
  t.notDeepEqual(submission.targetId, submission.sourceId, 'the targetId is a new profileId')
  t.notDeepEqual(submissionId2, submission.states[0].key, 'an update was published to the proposeEdit submission')

  t.deepEqual(
    submission,
    {
      key: submissionId2,
      type: 'submissions',
      originalAuthor: ssb.id,
      recps: null,
      groupId,
      sourceId: profileId,
      targetId: submission.targetId,
      targetType: 'profile/person',
      details: updateDetails,
      conflictFields: [],
      tombstone: null,
      approvedBy: [ssb.id],
      rejectedBy: [],
      comments: {
        [ssb.id]: 'Yup'
      },
      states: [
        {
          key: submission.states[0].key,
          targetId: submission.targetId,
          tombstone: null,
          approvedBy: [ssb.id],
          rejectedBy: [],
          comments: {
            [ssb.id]: 'Yup'
          }
        }
      ]
    },
    'returns the correct submission after execute'
  )

  profile = await p(ssb.profile.person.public.get)(profileId)

  t.deepEquals(
    profile,
    {
      key: profileId,
      type: 'profile/person',
      originalAuthor: ssb.id,
      recps: null,

      ...approvedRecordDetails,
      ...updateDetails,

      conflictFields: [],
      states: [
        {
          key: submission.targetId,
          tombstone: null,
          ...approvedRecordDetails,
          ...updateDetails,
          avatarImage: null,
          authors: profile.authors,
          type: 'person'
        }
      ],
      tombstone: null,
      avatarImage: null,
      authors: profile.authors
    },
    'the profile was updated'
  )
})

test('executeAndApprove a submission that has already been approved', async t => {
  t.plan(3)
  const ssb = Server()

  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const recordDetails = {
    preferredName: 'Cherese'
  }

  const submissionId = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, recordDetails, {})

  await p(ssb.submissions.executeAndApprove)(submissionId, recordDetails, {})
    .then(() => t.pass('Execute and approve the submission'))
    .catch(() => t.fail('Expected executeAndApprove to pass'))

  // attempt to execute and approve again
  await p(ssb.submissions.executeAndApprove)(submissionId, recordDetails, {})
    .then(() => t.fail('Expected executeAndApprove to fail'))
    .catch(err => {
      t.pass('Execute and approve failed as expected')

      t.match(
        err.message,
        /has already been approved/,
        'returns expected error message'
      )
    })
})

test('executeAndApprove a submission that has already been rejected', async t => {
  t.plan(2)
  const ssb = Server()

  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const recordDetails = {
    preferredName: 'Cherese'
  }

  const submissionId = await p(ssb.submissions.proposeNew)({ type: 'profile/person' }, recordDetails, {})

  await p(ssb.submissions.reject)(submissionId, {})

  // attempt to execute and approve after rejecting
  await p(ssb.submissions.executeAndApprove)(submissionId, recordDetails, {})
    .then(() => t.fail('Expected executeAndApprove to fail'))
    .catch(err => {
      t.pass('Execute and approve failed as expected')

      t.match(
        err.message,
        /has already been rejected/,
        'returns expected error message'
      )
    })
})

test('executeAndApprove - cant approve dependent submission', async t => {
  t.plan(1)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  kaitiaki.submissions.registerHandler(kaitiaki.profile.person.group)
  kaitiaki.submissions.registerHandler(CrutByType(kaitiaki)('link/profile-profile/child'))

  member.submissions.registerHandler(member.profile.person.group)
  member.submissions.registerHandler(CrutByType(member)('link/profile-profile/child'))

  // 1. Kaitiaki creates a profile as the "parent" in the relationship
  const parentInput = {
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const mumsProfileId = await p(kaitiaki.profile.person.group.create)(parentInput)

  // 2. Member proposes to add a new "child" to the "parent"
  const submissionIdA = await p(member.submissions.proposeNew)(
    { type: 'profile/person' },
    {
      preferredName: 'Cherese',
      authors: {
        add: ['*']
      },
      recps: [groupId]
    },
    {
      groupId,
      comment: 'I want to add my child'
    }
  )

  const linkDetails = {
    parent: mumsProfileId,
    // NOTE: we leave this empty on purpose, and the value will be filled in later
    // this is classed as a dependent field.. TODO: how do we flag a field as dependent
    // child: undefined
    relationshipType: 'birth',
    legallyAdopted: false
  }

  const submissionIdB = await p(member.submissions.proposeNew)(
    { type: 'link/profile-profile/child' },
    linkDetails,
    {}
  )

  await p(member.submissions.link.create)(
    submissionIdA, // parent
    submissionIdB, // child
    {} // opts
  )

  await member._replicate(kaitiaki)
  await kaitiaki._replicate(member)

  // attempt to approve the dependent submission
  await p(kaitiaki.submissions.executeAndApprove)(submissionIdB, linkDetails, {})
    .then(() => t.fail('Should have thrown an error'))
    .catch(err => {
      t.deepEqual(
        err.message,
        `submissions.execute Submission with id: ${submissionIdB} is dependent on the submission with id: ${submissionIdA} being approved`,
        'threw an error when trying to approve a dependent submission where the parent hasnt been approved'
      )
    })
})

test('executeAndApprove - approve submission with dependent submission(s)', async t => {
  t.plan(2)

  const {
    kaitiaki,
    member,
    groupId
  } = await TestGroup()

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  kaitiaki.submissions.registerHandler(kaitiaki.profile.person.group)
  kaitiaki.submissions.registerHandler(CrutByType(kaitiaki)('link/profile-profile/child'))

  member.submissions.registerHandler(member.profile.person.group)
  member.submissions.registerHandler(CrutByType(member)('link/profile-profile/child'))

  // 1. Kaitiaki creates a profile as the "parent" in the relationship
  const parentInput = {
    preferredName: 'Claudine',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  const mumsProfileId = await p(kaitiaki.profile.person.group.create)(parentInput)

  const profileDetails = {
    preferredName: 'Cherese',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }

  // 2. Member proposes to add a new "child" to the "parent"
  const submissionIdA = await p(member.submissions.proposeNew)(
    { type: 'profile/person' },
    profileDetails,
    {
      groupId,
      comment: 'I want to add my child'
    }
  )

  const linkDetails = {
    parent: mumsProfileId,
    // child = left empty because we dont have this yet
    relationshipType: 'birth',
    legallyAdopted: false
  }

  const submissionIdB = await p(member.submissions.proposeNew)(
    { type: 'link/profile-profile/child' },
    linkDetails,
    {
      groupId,
      comment: 'I want to add my child'
    }
  )

  await p(member.submissions.link.create)(
    submissionIdA,
    submissionIdB,
    {
      mappedDependencyFields: {
        child: 'targetId'
      }
    } // opts
  )

  await member._replicate(kaitiaki)
  await kaitiaki._replicate(member)

  // 1. executeAndApprove the parent submission
  await p(kaitiaki.submissions.executeAndApprove)(submissionIdA, profileDetails, {})
    .then(() => t.pass('executeAndApprove parent submission'))

  // 2. executeAndApprove the dependent submission
  await p(kaitiaki.submissions.executeAndApprove)(submissionIdB, linkDetails, {})
    .catch(err => {
      console.log(err)
    })

  const submissions = await kaitiaki.submissions.list()

  const expectedSubmissions = [
    {
      key: submissionIdB,
      type: 'submissions',
      originalAuthor: member.id,
      recps: null,
      sourceId: null,
      targetType: 'link/profile-profile/child',
      groupId,
      details: linkDetails,
      conflictFields: [],
      states: submissions[0].states, // hack
      tombstone: null,
      targetId: submissions[0].targetId, // new
      approvedBy: [kaitiaki.id],
      rejectedBy: [],
      comments: {
        [member.id]: 'I want to add my child'
      }
    },
    {
      key: submissionIdA,
      type: 'submissions',
      originalAuthor: member.id,
      recps: null,
      sourceId: null,
      targetType: 'profile/person',
      groupId,
      details: profileDetails,
      conflictFields: [],
      states: submissions[1].states,
      tombstone: null,
      targetId: submissions[1].targetId,
      approvedBy: [kaitiaki.id],
      rejectedBy: [],
      comments: {
        [member.id]: 'I want to add my child'
      }
    }
  ]

  t.deepEqual(
    submissions,
    expectedSubmissions,
    'returns the submissions'
  )
})
