const test = require('tape')
const { promisify: p } = require('util')
const { isMsgId } = require('ssb-ref')

const Server = require('./test-bot')

const details = () => ({
  preferredName: 'Colin'
})

test('proposeEdit simple', t => {
  t.plan(14)

  const ssb = Server()
  t.teardown(ssb.close)

  const profileDetails = {
    preferredName: 'Col',
    authors: { add: [ssb.id] }

  }
  ssb.submissions.registerHandler(ssb.profile.person.public)

  ssb.profile.person.public.create(profileDetails, (err, profileId) => {
    t.error(err, 'Profile created')

    ssb.submissions.proposeEdit(profileId, details(), { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' }, (err, submissionId) => {
      t.error(err, 'Submission created with no error')
      t.equal(typeof (submissionId), 'string', 'submissionId is a string')
      t.true(isMsgId(submissionId), 'submissionId is a messageId')

      ssb.submissions.get(submissionId, (err, data) => {
        t.error(err, 'No error when reading data')
        t.deepEqual(data.type, 'submissions', 'Submission has correct type')
        t.deepEqual(data.sourceId, profileId, 'Submission has correct sourceId')
        t.deepEqual(data.targetType, 'profile/person', 'Submission has correct targetType')
        t.deepEqual(data.details, details(), 'Submission has correct details')
        t.deepEqual(data.comments, { }, 'Submission has no comment')
        t.deepEqual(data.groupId, '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked', 'Submission returns the groupId')

        ssb.submissions.proposeEdit(profileId, {}, { groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked' }, (err, submissionId) => {
          t.error(err, 'Identity proposed with no error')
          ssb.submissions.get(submissionId, (err, data) => {
            t.error(err, 'No error when reading data')
            t.deepEqual(data.details, {}, 'Submission has empty details')
          })
        })
      })
    })
  })
})

/**
 * TODO cherese 29/03/23
 * Need to add tests for more cases:
 * - [ ] recordDetails
 *   - [ ] authors
 *   - [ ] recps
 * - [ ] submissionDetails
 *   - [ ] recps
 */
test('proposeEdit invalid field inputs (profile/person public)', async t => {
  t.plan(18)

  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  const profileId = await p(ssb.profile.person.public.create)({ authors: { add: [ssb.id] } })
  const communityProfileId = await p(ssb.profile.community.public.create)({ authors: { add: [ssb.id] } })

  const defaultValidFields = {
    recordId: profileId,
    recordDetails: {
      preferredName: 'Colin'
      // authors: { add: [ssb.id] }
    },
    submissionDetails: {
      groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
    }
  }

  /**
   * Failing test cases
   */

  const failCases = [
    // invalid recordId cases
    {
      recordId: null,
      expectedError: /submissions.proposeEdit recordId is missing/
    },
    {
      recordId: undefined,
      expectedError: /submissions.proposeEdit recordId is missing/
    },
    {
      recordId: false,
      expectedError: /submissions.proposeEdit recordId is missing/
    },
    { // invalid string value for recordId
      recordId: 'not a valid recordId',
      expectedError: /submissions.proposeEdit recordId must be a valid msgId/
    },
    { // invalid array value for recordId
      recordId: ['not a valid recordId'],
      expectedError: /submissions.proposeEdit recordId must be a valid msgId/
    },
    { // invalid object value for recordId
      recordId: { recordId: 'not a valid recordId' },
      expectedError: /submissions.proposeEdit recordId must be a valid msgId/
    },

    // valid recordId, but doesnt exist
    {
      recordId: '%R+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256',
      expectedError: /submissions.proposeEdit no record found for recordId: %R\+orr5I0h2eylYGpnXZKYgV8Csh0fpaFjo33k2LPQbg=.sha256/
    },

    // valid recordId, but no handler registered
    {
      recordId: communityProfileId,
      expectedError: /submissions.proposeEdit Error: No CRUT registered for type profile\/community/
    },

    // invalid recordDetails cases
    {
      recordDetails: null,
      expectedError: /submissions.proposeEdit recordDetails must be an object/
    },
    {
      recordDetails: undefined,
      expectedError: /submissions.proposeEdit recordDetails must be an object/
    },
    {
      recordDetails: false,
      expectedError: /submissions.proposeEdit recordDetails must be an object/
    },

    // valid profileId, invalid details
    {
      recordDetails: ['not valid details'],
      expectedError: /submissions.proposeEdit recordDetails must be an object/
    },
    {
      recordDetails: { causes: 'error' },
      expectedError: /submissions.proposeEdit recordDetails are invalid for the type: profile\/person/
    },

    // profile/person recordDetails - validate against spec
    // TODO: this only tests the profile/person case, we may need to test other crut types?
    {
      recordDetails: {
        preferredName: 'Cherese',
        legalName: 'Cherese Eriepa' // invalid against the profile/person public spec
      },
      expectedError: /submissions.proposeEdit recordDetails are invalid for the type: profile\/person/
    },

    // invalid submissionDetails
    {
      submissionDetails: null,
      expectedError: /submissions.proposeEdit submissionDetails must be an object/
    },
    {
      submissionDetails: undefined,
      expectedError: /submissions.proposeEdit submissionDetails must be an object/
    },
    {
      submissionDetails: false,
      expectedError: /submissions.proposeEdit submissionDetails must be an object/
    },
    {
      submissionDetails: { dog: 'coco' },
      expectedError: /submissions.proposeEdit submissionDetails has unallowed inputs: dog/
    }
    // {
    //   submissionDetails: {},
    //   expectedError: /data.groupId is required/
    // }
  ]

  /**
   * Run the failing test cases
   */

  await Promise.all(
    failCases.map(async input => {
      const {
        recordId,
        recordDetails,
        submissionDetails,
        expectedError
      } = ({ ...defaultValidFields, ...input })

      delete input.expectedError

      return p(ssb.submissions.proposeEdit)(recordId, recordDetails, submissionDetails)
        .then(() => t.fail(`expected ${expectedError} to fail, but it passed instead`))
        .catch(err => t.match(err.message, expectedError, `submission.proposeEdit invalid input ${JSON.stringify(input)} throws error`))
    })
  )
})
