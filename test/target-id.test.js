const { promisify: p } = require('util')
const test = require('tape')

const Server = require('./test-bot')
const { sleep } = require('./lib/helpers')

test('targetId', async t => {
  const kaitiaki = Server({ tribes: true })
  const member = Server({ tribes: true })

  t.teardown(() => {
    kaitiaki.close()
    member.close()
  })

  kaitiaki.submissions.registerHandler(kaitiaki.profile.person.group)
  member.submissions.registerHandler(member.profile.person.group)

  // kaitiaki sets up the group
  const { groupId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  // kaitiaki adds the member to the group
  await p(kaitiaki.tribes.invite)(groupId, [member.id], {})

  await kaitiaki._replicate(member)
  await member._replicate(kaitiaki)

  // wait for rebuild
  await sleep(500)

  // kaitiaki creates a profile in the group
  const input = {
    preferredName: 'David',
    altNames: { add: ['James'] },
    legalName: 'David James',
    authors: {
      add: ['*']
    },
    recps: [groupId]
  }
  const profileId = await p(kaitiaki.profile.person.group.create)(input)
  await kaitiaki._replicate(member)

  // member proposes an update to that profile
  const defnTime = Date.now()
  const updateDetails = {
    preferredName: 'Dave',
    legalName: null,
    customFields: {
      [defnTime]: 'hamburgers'
    },
    altNames: {
      add: ['Davey', 'David'],
      remove: ['James']
    }
  }

  const submissionId = await p(member.submissions.proposeEdit)(profileId, updateDetails, { groupId, recps: [poBoxId, member.id] })
  await member._replicate(kaitiaki)

  let expected = {
    key: submissionId,
    type: 'submissions',
    originalAuthor: member.id,
    recps: [poBoxId, member.id],
    sourceId: profileId,
    targetType: 'profile/person',
    groupId,
    details: {
      preferredName: 'Dave',
      legalName: null,
      customFields: { [defnTime]: 'hamburgers' },
      altNames: {
        add: ['Davey', 'David'],
        remove: ['James']
      }
    },
    conflictFields: [],
    states: [
      {
        key: submissionId,
        tombstone: null,
        targetId: null,
        approvedBy: [],
        rejectedBy: [],
        comments: {}
      }
    ],
    tombstone: null,
    targetId: null,
    approvedBy: [],
    rejectedBy: [],
    comments: {}
  }

  // kaitiaki sees the submission
  let submission = await p(kaitiaki.submissions.get)(submissionId)
  t.deepEqual(submission, expected, 'kaitiaki can see the submission')

  // member sees the submission
  submission = await p(member.submissions.get)(submissionId)
  t.deepEqual(submission, expected, 'member can see the submission')

  // kaitiaki publishes the requested change
  const updateId = await p(kaitiaki.profile.person.group.update)(profileId, updateDetails)
  t.notEqual(profileId, updateId, 'the profiles original ID isnt the same as the id returned on update')

  // kaitiaki approves the submission and adds the pointer to the update message for the profile
  const submissionUpdateId = await p(kaitiaki.submissions.approve)(submissionId, { targetId: updateId })
  await kaitiaki._replicate(member)

  expected = {
    key: submissionId,
    type: 'submissions',
    originalAuthor: member.id,
    recps: [poBoxId, member.id],
    sourceId: profileId,
    targetType: 'profile/person',
    groupId,
    details: {
      preferredName: 'Dave',
      legalName: null,
      customFields: { [defnTime]: 'hamburgers' },
      altNames: {
        add: ['Davey', 'David'],
        remove: ['James']
      }
    },
    conflictFields: [],
    states: [
      {
        key: submissionUpdateId,
        tombstone: null,
        targetId: updateId,
        approvedBy: [kaitiaki.id],
        rejectedBy: [],
        comments: {}
      }
    ],
    tombstone: null,
    targetId: updateId,
    approvedBy: [kaitiaki.id],
    rejectedBy: [],
    comments: {}
  }

  // kaitiaki sees the updated submission
  submission = await p(kaitiaki.submissions.get)(submissionId)
  t.deepEqual(submission, expected, 'kaitiaki can see the updated submission')

  // member sees the submission
  submission = await p(member.submissions.get)(submissionId)
  t.deepEqual(submission, expected, 'member can see the updated submission')

  let profileUpdate = await p(RenderUpdate(kaitiaki))(updateId)
  t.deepEqual(profileUpdate, expected.details, 'kaitiaki can see the requested fields were updated')

  profileUpdate = await p(RenderUpdate(member))(updateId)
  t.deepEqual(profileUpdate, expected.details, 'member can see the requested fields were updated')
})

function RenderUpdate (ssb) {
  return function renderUpdate (id, cb) {
    ssb.get({ id, private: true }, (err, val) => {
      if (err) return cb(err)

      const crut = ssb.submissions.findHandler(val.content)
      if (!crut) return cb(Error('no crut handler registered for type ' + val.content.type))

      const { content } = val
      const output = crut.strategy.mapToOutput(content)

      const result = Object.keys(content).reduce((acc, key) => {
        switch (key) {
          case 'type':
          case 'recps':
          case 'tangles':
            return acc

          case 'altNames':
            acc[key] = Object.entries(content[key]).reduce(
              (acc, [key, value]) => {
                value > 0
                  ? acc.add.push(key)
                  : acc.remove.push(key)
                return acc
              },
              { add: [], remove: [] }
            )
            return acc

          default:
            acc[key] = output[key]
            return acc
        }
      }, {})

      cb(null, result)
    })
  }
}
