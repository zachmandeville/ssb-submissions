const Server = require('scuttle-testbot')
const { replicate } = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   tribes: Boolean
  // }

  var stack = Server // eslint-disable-line
    // .use(require('ssb-private1'))
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('../')) // ssb-submissions

  if (opts.tribes || opts.recpsGuard) {
    stack = stack
      .use(require('ssb-tribes'))
    // only add ssb-tribes when testing recps, as it keystore startup takes 500ms
  }

  if (opts.recpsGuard === true) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  const ssb = stack(opts)

  ssb.name = `testbot-${(3000 + Math.random() * 7000 | 0)}`

  ssb._replicate = async (to) => {
    return replicate({
      from: ssb,
      to,
      name: id => id === ssb.id ? opts.name : to.name,
      log: false
    })
  }

  return ssb
}
