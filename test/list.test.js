const test = require('tape')
const { promisify: p } = require('util')

const Server = require('./test-bot')

const generateSubmission = (preferredName) => ({
  recordTypeContent: { type: 'profile/person' },
  recordDetails: {
    preferredName
    // authors: { add: [ssb.id] }
  },
  submissionDetails: {
    comment: `update name ${preferredName}`,
    groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked'
  }
})

test('submissions.list', async t => {
  t.plan(1)

  /**
   * Setup
   */
  const ssb = Server()
  t.teardown(ssb.close)

  ssb.submissions.registerHandler(ssb.profile.person.public)

  /**
   * Test submissions
   */

  const newSubmissions = [
    generateSubmission('Cherese'),
    generateSubmission('Mix'),
    generateSubmission('Ben'),
    generateSubmission('Colin')
  ]

  /**
   * Create a bunch of submissions
   */

  const expectedSubmissions = await Promise.all(
    newSubmissions.map(async input => {
      const {
        recordTypeContent,
        recordDetails,
        submissionDetails
      } = input

      const submissionId = await p(ssb.submissions.proposeNew)(recordTypeContent, recordDetails, submissionDetails)

      return {
        key: submissionId,
        type: 'submissions',
        originalAuthor: ssb.id,
        recps: null,
        sourceId: null,
        targetId: null,
        groupId: '%5Z1sMdztpcwWqy9USpVzVUnA+4mfpOG59KNPXVyHmZ4=.cloaked',
        targetType: 'profile/person',
        details: { preferredName: recordDetails.preferredName },
        conflictFields: [],
        states: [
          {
            key: submissionId,
            targetId: null,
            tombstone: null,
            approvedBy: [],
            rejectedBy: [],
            comments: {
              [ssb.id]: `update name ${recordDetails.preferredName}`
            }
          }
        ],
        tombstone: null,
        approvedBy: [],
        rejectedBy: [],
        comments: {
          [ssb.id]: `update name ${recordDetails.preferredName}`
        }
      }
    })
  )

  const submissions = await p(ssb.submissions.list)()

  t.deepEquals(submissions, expectedSubmissions.reverse(), 'lists all the submissions')
})
