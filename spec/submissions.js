const { feedIdSet, feedIdComments, messageIdProp } = require('../lib/field-types')
const { cloakedMessageId, messageId } = require('ssb-schema-definitions')()

const Spec = {
  type: 'submissions', // Maybe include new/edit/tombstone
  tangle: 'submissions',
  staticProps: {
    // Static to avoid any changes after approveance/rejection
    sourceId: {
      type: ['string', 'null'],
      pattern: messageId.pattern
    },
    targetType: { type: 'string', required: true },
    groupId: {
      type: 'string',
      pattern: cloakedMessageId.pattern
      // required: true TODO: cant be required because of public profiles
    },
    details: { type: 'object', required: true } // TODO: rename details?
  },
  props: {
    targetId: messageIdProp,
    approvedBy: feedIdSet,
    rejectedBy: feedIdSet,
    comments: feedIdComments
    // A pointer to the target record update/create message once it has been created
  },
  hooks: {
    // isRoot: [
    //   DateChecker('createdAt')
    // ],
    // isUpdate: [
    //
    // ]
  }

  // TODO: A person shouldn't be able to approve their own submissions
  // Using isValidNextStep
}

module.exports = Spec
