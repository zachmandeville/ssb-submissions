module.exports = {
  type: 'link/submissions',
  staticProps: {
    parent: { $ref: '#/definitions/messageId', required: true },
    child: { $ref: '#/definitions/messageId', required: true },
    mappedDependencyFields: {
      nullable: true,
      type: 'object',
      patternProperties: {
        '^[a-zA-Z]+$': { type: 'string' }
      }
    }
  },
  props: {}
}
