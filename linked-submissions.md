# Linked Submissions

When we have submissions which depend on the result of another (when its executed), we call these dependent submissions.

## API

### `server.submissions.link.create(parentId, childId, opts, cb)`

Creates a link messaged between `parentId` and `childId`.

* `parentId` *Object* - messageId of a submission. The `parentId` should be the submission that is **depended on** by another submission.
* `childId` *Object* - messageId of a submission. The `childId` should be the submission that is **dependent on** another submission specified as the `parentId`
* `opts` *Object* - optional details you can add to the link
  * `mappedDependencies` *Object* - specify which fields on the submission with `childId` should be replaced with the field from the submission with `parentId`. See example below for how mappedDependencies is works
  `{ [key]: value, ..., [keyN]: valueN }`
* `cb` *Function* - a callback with signature (err, submissionId)

### `server.submissions.link.get(linkId, cb)`

see the `read` method in [ssb-crut/read](https://gitlab.com/ahau/lib/ssb-crut/-/blob/master/index.js#L119)


### `server.submissions.link.list(opts, cb)`

see the `list` method in [ssb-crut/list](https://gitlab.com/ahau/lib/ssb-crut/-/blob/master/index.js#L209)

## Dependent Submissions

Here is an example of how a submission becomes dependent on another:

1. `SubmissionA` - A user creates a submission to **create a new profile** for a person named `Cherese`
2. `SubmissionB` - A user creates a submission to **create a link** from Cherese to an already existing person `Claudine`
3. `Link` - The users links `SubmissionA` and `SubmissionB` together

E.g:
- `SubmissionB` now depends on `SubmissionA`
- `SubmissionB` cannot be executed until `SubmissionA` is executed
- `SubmissionB` cannot be approved until `SubmissionA` is approved
- Once `SubmissionA` is executed and approved, THEN `SubmissionB` can be executed and approved

## Mapped Dependencies

A mapped dependency for this example is:

```
const mappedDependencies = {
  [childField]: [parentField]
}


server.submissions.link.create(submissionA, submissionB, { mappedDependencies }, cb)
```

Where: 
- `childField` is the field on `SubmissionB` (the child submission)
- `parentField` is the field on `SubmissionA` (the parent submission)
- When executing `SubmissionB`, it will map `childField` to the value of `parentField` on `SubmissionA`. This will usually be the `targetId` which is the result of a `create` or `update` of the record
