const { isMsgId } = require('ssb-ref')
const validateSubmissionDetails = require('../lib/validate-submission-details')

module.exports = function reject (ssb, submissionCrut) {
  return function reject (submissionId, submissionDetails, cb) {
    const done = (err, data) => err
      ? cb(new Error(`submissions.reject ${err}`))
      : cb(null, data)

    if (!submissionId) return done('submissionId is missing')
    if (!isMsgId(submissionId)) return done('submissionId must be a valid msgId')

    validateSubmissionDetails(submissionDetails, (err) => {
      if (err) return done(err)

      const { comment } = submissionDetails

      ssb.get({ id: submissionId }, (err) => {
        if (err) {
          if (err.message.match(/Key not found in database/)) return done(`no submission found for submissionId: ${submissionId}`)
          return done(err)
        }

        const content = {
          rejectedBy: { add: [ssb.id] }
        }

        if (comment) content.comments = { [ssb.id]: comment }
        submissionCrut.update(submissionId, content, cb)
      })
    })
  }
}
