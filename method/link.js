const Crut = require('ssb-crut')

const LinkSpec = require('../spec/link')

function Link (ssb) {
  const linkCrut = new Crut(ssb, LinkSpec)

  function validateSubmission (id, cb) {
    ssb.get({ id, private: true }, (err, root) => {
      if (err) return cb(err)

      if (root.content.type === 'submissions') cb(null, root)
      else cb(new Error(`Expected ${id} to be of type submissions, got ${root.content.type} instead.`))
    })
  }

  return {
    get (linkId, cb) {
      return linkCrut.read(linkId, cb)
    },
    create: (parentId, childId, opts = {}, cb) => {
      validateSubmission(parentId, (err, root) => {
        if (err) return cb(err)

        validateSubmission(childId, (err) => {
          if (err) return cb(err)

          const content = {
            parent: parentId,
            child: childId
          }

          if (opts.mappedDependencyFields) content.mappedDependencyFields = opts.mappedDependencyFields

          // TODO: recps
          if (root.content.recps) content.recps = root.content.recps

          linkCrut.create(content, cb)
        })
      })
    },
    list (opts, cb) {
      if (typeof opts === 'function') return linkCrut.list({}, opts)
      return linkCrut.list(opts, cb)
    }
  }
}

module.exports = Link
