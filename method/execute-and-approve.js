const { isMsgId } = require('ssb-ref')
const get = require('lodash.get')
const Approve = require('./approve')

module.exports = function ExecuteAndApprove (ssb, FindRecordCrut, submissionCrut) {
  const approve = Approve(ssb, submissionCrut)

  function createOrUpdateRecord (submissionId, approvedRecordDetails, done) {
    submissionCrut.read(submissionId, (err, submission) => {
      if (err) {
        if (err.message.match(/Key not found in database/)) return done(`no submission found for submissionId: ${submissionId}`)
        return done(err)
      }

      if (isApproved(submission)) return done(`Submission with id: ${submissionId} has already been approved.`)
      if (isRejected(submission)) return done(`Submission with id: ${submissionId} has already been rejected.`)

      mapDependencyFields(submission, approvedRecordDetails, (err, details) => {
        if (err) return done(err)

        const recordCrut = FindRecordCrut({ type: submission.targetType })
        if (!recordCrut) return done('No handler found for the recordTypeContent: ' + JSON.stringify(submission.targetType))

        if (submission.sourceId) return recordCrut.update(submission.sourceId, approvedRecordDetails, done)

        const content = {
          ...details
        }

        // NOTE: some specs do not allow authors i.e. links
        if (get(recordCrut, 'spec.props.authors') && !content.authors) {
          content.authors = {
            add: ['*']
          }
        }

        recordCrut.create(content, done)
      })
    })
  }

  // here we check if the submission has any
  function mapDependencyFields (submission, approvedRecordDetails, done) {
    const submissionId = submission.key
    ssb.submissions.link.list(
      {
        filter: (link) => (
          link.child === submissionId &&

          // TODO: for now, we only allow links from the same author
          link.originalAuthor === submission.originalAuthor
        )
      }, (err, parentLinks) => {
        if (err) return done(err)

        // if the submission has no parent submissions, then carry on
        if (parentLinks.length === 0) return done(null, approvedRecordDetails)

        // if it does, then we need to check the result of the submissions first
        // before proceeding
        if (parentLinks.length !== 1) return done(`Submission with the id: ${submissionId} is dependent on multiple submissions. This is not supported yet.`)

        const {
          parent: parentSubmissionId,
          mappedDependencyFields
        } = parentLinks[0]

        submissionCrut.read(parentSubmissionId, (err, parentSubmission) => {
          if (err) return done(err)

          if (!isApproved(parentSubmission)) return done(`Submission with id: ${submissionId} is dependent on the submission with id: ${parentSubmissionId} being approved`)
          if (isRejected(parentSubmission)) return done(`Submission with id: ${submissionId} is dependent on the submission with id: ${parentSubmissionId}, but that submission was rejected`)

          const details = { ...approvedRecordDetails }

          // replace fields using the mappings provided
          if (mappedDependencyFields) {
            Object.entries(mappedDependencyFields)
              .forEach(([key, value]) => {
                details[key] = parentSubmission[value]
              })
          }

          done(null, details)
        })
      }
    )
  }

  return function executeAndApprove (submissionId, approvedRecordDetails = {}, submissionDetails = {}, cb) {
    const done = (err, data) => err
      ? (typeof err === 'string')
          ? cb(new Error(`submissions.execute ${err}`))
          : cb(err)
      : cb(null, data)

    if (!submissionId) return done('submissionId is missing')
    if (!isMsgId(submissionId)) return done('submissionId must be a valid msgId')

    createOrUpdateRecord(submissionId, approvedRecordDetails, (err, updateId) => {
      if (err) return done(err)

      // approve the submission and save the targetId to it
      submissionDetails.targetId = updateId

      approve(submissionId, submissionDetails, done)
    })
  }
}

function isApproved (submission) {
  return Boolean(get(submission, 'approvedBy.length'))
}

function isRejected (submission) {
  return Boolean(get(submission, 'rejectedBy.length'))
}
