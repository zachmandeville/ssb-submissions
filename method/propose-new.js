const isObject = require('../lib/is-object')
const validateRecordDetails = require('../lib/validate-record-details')
const validateSubmissionDetails = require('../lib/validate-submission-details')

module.exports = function ProposeNew (ssb, findRecordCrut, submissionCrut) {
  function validateRecord (recordTypeContent, recordDetails, done) {
    if (!recordTypeContent) return done('recordTypeContent is missing')
    if (!isObject(recordTypeContent)) return done('recordTypeContent must be an object')

    const recordCrut = findRecordCrut(recordTypeContent)
    if (!recordCrut) return done('No handler found for the recordTypeContent: ' + JSON.stringify(recordTypeContent))

    validateRecordDetails(recordDetails, recordCrut, done)
  }

  return function proposeNew (recordTypeContent, recordDetails, submissionDetails, cb) {
    const done = (err, data) => err
      ? cb(new Error(`submissions.proposeNew ${err}`))
      : cb(null, data)

    validateRecord(recordTypeContent, recordDetails, (err) => {
      if (err) return done(err)

      validateSubmissionDetails(submissionDetails, (err) => {
        if (err) return done(err)

        const { comment, recps, groupId } = submissionDetails

        const { type } = recordTypeContent

        const content = {
          // sourceId: '',
          // targetId: '',
          targetType: type,

          details: recordDetails, // TODO: Recps for final record in here

          groupId, // Keep a copy of the groupId, required
          recps // Who is allowed to see this submission. The targetRecord may have different recps.
          // TODO: Submissions should only be seen by kaitiaki group
        }

        if (comment) content.comments = { [ssb.id]: comment }
        submissionCrut.create(content, cb)
      })
    })
  }
}
