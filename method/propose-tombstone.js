const ProposeEdit = require('./propose-edit')

module.exports = function ProposeTombstone (ssb, loadCrut, submissionCrut) {
  const proposeEdit = ProposeEdit(ssb, loadCrut, submissionCrut)

  return function proposeTombstone (recordId, submissionDetails, cb) {
    const recordDetails = {
      tombstone: {
        date: Date.now()
      }
    }

    proposeEdit(recordId, recordDetails, submissionDetails, (err, data) => {
      if (err) return cb(new Error(`submissions.proposeTombstone ${err}`))

      cb(null, data)
    })
  }
}
